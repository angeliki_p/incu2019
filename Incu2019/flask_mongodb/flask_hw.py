# Import necessary libraries

from flask import Flask
from flask import render_template, request, jsonify, abort
from pymongo import MongoClient
import json

# Create and connect to Mongo Database called "ADDRESS_BOOK". Our collection is called "USERS"

# USERS contain : userid, name, phone number, email

client = MongoClient('localhost', 27017)
db = client.ADDRESS_BOOK
col = db.USERS

# Create new Flask application

app = Flask(__name__)


# This is the initial page the user sees when goes to http://localhost:7676/

@app.route("/")
def index():

    """This method returns a message to indicate we are in the initial page
    of the address book"""

    return "Welcome to Address Book!"


@app.route("/addusers", methods=['POST'])
def add_user():

    """ This method adds a new user in the Mongo DB according to the data given
    in the REST POST request."""

    # Gets the data from the REST request

    data = json.loads(request.data)

    # Extracts the name of the request for a comprehensive message

    name = data['name']

    """If insertion is successful returns a success message
    with the name associated with the entry, otherwise it returns a fail message
    with details of what exactly went wrong using the Exception returned."""

    try:

        col.insert_one(data)

        return jsonify(message="Entry for " + "'" + name + "'" + " was inserted successfully.")

    except Exception as e:

        return jsonify(message="Insertion Failed: " + str(e))


@app.route("/deleteusers", methods=['DELETE'])
def delete_user():

    """This method deletes a user from Mongo DB according to the data given
    in the REST DELETE request"""

    # Gets the data from the REST request

    data = json.loads(request.data)

    userid = data["user_id"]

    """Attempts to find the element with the specified user_id  in the MongoDB.
    If the entry is found, then the entry is deleted. If it is not found we return
     a 404 error: not found."""

    # Try to find the element specified

    try:

        entry = col.find_one({"user_id": userid})

        name = entry["name"]

        # Delete the element

        col.delete_one({"user_id": userid})

        # Return success message

        return jsonify(message="Entry for " + "'" + name + "'" + " was deleted successfully.")

    # If the element is not found return error 404

    except Exception:

        abort(404)


@app.route("/listall", methods=['GET'])
def list_entries():

    """ This method lists all entries that exist in the USERS collection of ADDRESS_BOOK.
    If the collection is empty then it returns a "The collection is empty" message.
    Otherwise, it returns all the collection  entries in JSON format."""

    # Check if collection is empty

    if col.count() == 0:

        return jsonify(entries="The collection is empty")

    else:

        # find all entries

        cursor = col.find()

        # create a list of dictionaries with the data from each entry

        entries = []

        for each in cursor:

            entries.append({"user_id": each['user_id'], "name": each['name'],
                            "phone": each['phone'], "email": each['email']})

        # Return the entries

        return jsonify(entries=entries)


@app.route("/listone", methods=['GET'])
def list_one():

    """ This method lists an element specified in a REST GET request
    in the USERS collection. If the element exists in the collection,
    it returns the data associated with the element in json format.
    Otherwise, it returns an error 404: Not found."""

    # Get request data

    data = json.loads(request.data)

    user_id = data['user_id']

    # Try to find the element in the collection

    try:

        entry = col.find_one({"user_id": user_id})

        name = entry['name']

        phone = entry['phone']

        email = entry['email']

        out = {"user_id": user_id, "name": name, "phone": phone, "email": email}

        return jsonify(entry=out)

    except Exception:

        abort(404)


@app.route("/updateone", methods=['PUT'])
def update_one():

    """ This method updates an element specified in a REST PUT request
    in the USERS collection. If the element exists in the collection,
    it updates the data associated with the element.
    Otherwise, it returns an error 404: Not found."""

    # Get the request data

    data = json.loads(request.data)

    user_id = data['user_id']

    """The data we want to update is already in json format, so in the $set for the
    update we can put it as is. We need everything else but the user_id to be
    updated so we delete it from the data dictionary."""

    del data['user_id']

    # Try to update the entry

    try:

        col.update({"user_id": user_id}, {'$set': data})

        # After the update, take the new data associated with the entry

        entry = col.find_one({"user_id": user_id})

        name = entry['name']

        phone = entry['phone']

        email = entry['email']

        out = {"user_id": user_id, "name": name, "phone": phone, "email": email}

        # Return the updated entry in json format

        return jsonify(updated_entry=out)

    # If the update cannot be done ( the entry does not exist) return error 404

    except Exception:

        abort(404)


# The flask app runs by default on port 5000, but we want it to run on port 7676


app.run(port=7676)
