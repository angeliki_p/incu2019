# Import necessary libraries

import pymongo as pm
from pprint import pprint

""" This script creates and address book in Mongo Database. Each separate
function is described in its individual block. """


def getmongo():
    """Creates a Mongo Database called 'incubator' and connects to the database.
    Creates a collection called 'ENTRIES' in the incubator database.
     In this collection, we are going to add information about people in the address
     book such as First name, Last name etc."""

    client = pm.MongoClient("localhost", 27017)
    db = client["incubator"]
    col = db.ENTRIES
    return col


def insert_new_entry(data: dict):
    """Connects to incubator Mongo Database. Given a dictionary of values about 
    an entry, it inserts these data to the database."""

    # Connection to database
    col = getmongo()

    # Insertion of values
    col.insert_one(data)

    """Optionally you can print the db contents to verify the insertion. Uncomment
    the next bunch of code to do that."""

    # cursor = col.find()
    # 
    # for each in cursor:
    #     pprint(each)


def update_entry(firstname: str, lastname: str, update_data: dict):
    
    """Connects to incubator Mongo database. Given the firstname, lastname and
    the data we would like to change in a database entry, this function changes 
    the data associated with an entry according to the input."""
    
    # Connect to database
    col = getmongo()
    
    # Query; Whose data are going to be changed
    query = {"firstname": firstname, "lastname": lastname}
    
    # The new values for the entry we want to change. 
    # We need a $set in order to update
    new_values = {"$set": update_data}
    
    # Update the entry
    col.update_one(query, new_values)

    """Optionally you can print the db contents to verify the update. Uncomment
        the next bunch of code to do that."""
    
    # cursor = col.find({"firstname": firstname, "lastname":lastname})
    # 
    # for each in cursor:
    #     pprint(each)


def display_entries(firstname=None, lastname=None):

    """This function displays the entries of the incubator Mongo Database.
    If the user gives no input (neither firstname or lastname), the function
    prints all the entries of the database sorted by lastname in ascending order.
    If the user gives only lastname then the function prints all the database entries
    with the exact or similar lastnames. Finally, if the user gives only firstname,
    the function prints all the database entries with the exact firstname or similar
    firstnames."""

    # Connect to database
    col = getmongo()

    # Database results according to user input
    if firstname is None and lastname is None:
        cursor = col.find().sort({"lastname": 1})
    elif firstname is None:
        cursor = col.find({"lastname": {"$regex": lastname + ".*"}})
    elif lastname is None:
        cursor = col.find({"firstname": {"$regex": firstname + ".*"}})

    # Print results
    for obj in cursor:
        pprint(obj)


def delete_entries(firstname=None, lastname=None):

    """This function deletes all the incubator database entries which
    match with the input given by the user."""

    # Connect to database
    col = getmongo()

    # Query ; Whose data are going to be deleted
    query = {"firstname": firstname, "lastname": lastname}

    # Delete entry
    col.delete_one(query)

    """Optionally you can print the db contents to verify the deletion. Uncomment
    the next bunch of code to do that."""

    # cursor = col.find()
    #
    # for each in cursor:
    #     pprint(each)

