# Import necessary libraries
from flask import Flask
from flask import render_template, request,redirect
from pymongo import MongoClient

# Create and connect to Mongo Database called "ADDRESS_BOOK". Our collection is called "USERS"

# USERS contain : userid, name, phone number, email

client = MongoClient('localhost', 27017)
db = client.ADDRESS_BOOK
col = db.USERS

# Create new Flask application

app = Flask(__name__)


# This is the initial page the user sees when goes to http://localhost:7676/<his/her name>

@app.route("/<name>")
def index(name):
    return render_template("index.html", name=name)

# This is the page the user sees when he wants to add an entry

@app.route("/add_entry")
def add_entry():
    return render_template("add_entry.html")
    

# This is the action when the user submits the form through the add_entry.html page / Inserts data to database and returns again to add_entry.html
# in case the user wants to add another entry.


@app.route("/add", methods=['POST'])
def add():
    user_id = request.values.get("id")
    name = request.values.get("name")
    email = request.values.get("email")
    phone = request.values.get("phone")
    col.insert({"user_id": user_id, "name": name, "phone": phone, "email": email})
    return redirect("/add_entry")


# This is the page the user sees when he wants to see all the entries on the database. It lists all the current entries of the database.


@app.route("/view")
def view():
    cursor = col.find()
    entries = []
    for each in cursor:
        entries.append({"user_id": each['user_id'], "name": each['name'],
                        "phone": each['phone'], "email": each['email']})
    return render_template('view_all.html', entries=entries)
    

# This is the page the user sees when he wants to delete an entry on the database. It asks for the entry id to be deleted.
    

@app.route("/delete_entry")
def delete_entry():
    return render_template("delete_entry.html")
    

# Deletes the entry and redirects user to view_all.html page to confirm the deletion of the entry specified.


@app.route("/delete", methods=['POST'])
def delete():
    user_id = request.values.get("id")
    col.delete_one({"user_id": user_id})
    return redirect("/view")


app.run(port=7676)
