# Import necessary libraries

import requests
import json


class Nexus:
    """ This class is meant to help with the automation of certain tasks for
     Nexus 9000v with the use of nxos REST API/ CLI API. The methods implemented in this
     class are:

     .authenticate() : Takes as input the username and password in order to create
     a session.

     .get_interface_status() : Takes as input the interface name and returns
     the status of the specified interface.

     .configure_interface_description() : Takes as input the interface name
     and the preferred description of the interface and changes its description.

     .get_version() : Does not get any input. Creates an attribute "version" for class
     Nexus. !!! NOTE !!! Before you get the attribute nexus.version, first you got to
     run authenticate() and get_version() prior.

     .get_platform() : Does not get any input. Creates an attribute "platform" for class
     Nexus. !!! NOTE !!! Before you get the attribute nexus.platform, first you got to
     run authenticate() and get_platform() prior.


     NOTE: the online version of nxos API is used.


    """
    version = None
    platform = None

    def __init__(self):

        # Create a persistent session
        self.session = requests.Session()

        # The url of the online sandbox
        self.url = "http://sbx-nxos-mgmt.cisco.com/"

        self.header = {'Content-Type': 'application/json'}

        self.password = ""

        self.username = ""

    def authenticate(self, username: str, password: str):

        url = self.url + "api/aaaLogin.json"

        self.username = username

        self.password = password

        payload = {
            "aaaUser": {
                "attributes": {
                    "name": username,
                    "pwd": password
                }
            }
        }
        response = self.session.post(url, data=json.dumps(payload), headers=self.header)

        if response.status_code == 200:

            print("Successful authentication!")

        else:

            print("Something went wrong :( Please try again.")

    def get_interface_status(self, interface_name: str):

        url = self.url + "ins"

        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show interface " + interface_name,
                "output_format": "json"
            }
        }

        response = self.session.post(url, data=json.dumps(payload), headers=self.header,
                                     auth=(self.username, self.password)).json()
        print("The interface " + interface_name + " is: " +
              response["ins_api"]["outputs"]["output"]["body"]["TABLE_interface"]["ROW_interface"]["state"])

    def configure_interface_description(self, interface_name: str, description: str):

        """" The interface given may be "Ethernet x/x, Ethernetx/x, ethx/x". Either way we need to
        convert it in "ethx/x" format in order to put it in the url."""


        if_name = interface_name[:3].lower() + interface_name[-3:]

        url = self.url + "/api/node/mo/sys/intf/phys-[" + if_name + "].json"

        payload = {"l1PhysIf": {"attributes": {"descr": description}}}

        response = self.session.put(url, data=json.dumps(payload), headers=self.header,
                                    auth=(self.username, self.password))

        if response.status_code == 200:

            print("You configured interface " + interface_name + " with description '" + description + "' successfully")

        else:

            print("Oops! Something went wrong :( Please try again!")

    def get_version(self):

        url = self.url + "ins"

        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show version",
                "output_format": "json"
            }
        }

        response = self.session.post(url, data=json.dumps(payload), headers=self.header,
                                     auth=(self.username, self.password)).json()

        self.version = response["ins_api"]["outputs"]["output"]["body"]["kickstart_ver_str"]

    def get_platform(self):

        url = self.url + "ins"

        payload = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show version",
                "output_format": "json"
            }
        }

        response = self.session.post(url, data=json.dumps(payload), headers=self.header,
                                     auth=(self.username, self.password)).json()

        self.platform = response["ins_api"]["outputs"]["output"]["body"]["chassis_id"]


if __name__ == "__main__":

    nexus = Nexus()
    nexus.authenticate("admin", "Admin_1234!")
    nexus.get_version()
    nexus.get_platform()
    print(nexus.version)
    print(nexus.platform)
    nexus.get_interface_status("Ethernet 1/1")
    nexus.configure_interface_description("Ethernet 1/3", "I'm playing around here")
