###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the text input will look like:
#
# --------------------------------------------------------------------------------
# Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# Eth1/3                0          0          0          0          0           0
# Eth1/4                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
# --------------------------------------------------------------------------------
# Eth1/1                0          0          0          0          0           0
# Eth1/2                0          0          0          0          0           0
# <snip>
#
# --------------------------------------------------------------------------------
# Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
# --------------------------------------------------------------------------------
# Eth1/1             0          --           0           0           0          0
# Eth1/2             0          --           0           0           0          0
#
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################

import regex

"""NOTE: The input was modified with ascending values in order to understand where goes what."""


def get_error_counters(text):
    """Gets the error counters of the input and return a dictionary with
    the error counters of each interface."""

    # The regular expression to get the error counters

    crc = regex.findall('((?<!\n)\w(\w+[-\w]\w+)(?!\/|\d|>|p))', text)
    crc_counters = [x[0] for x in crc]

    print(crc_counters)

    # The regular expression to get the interfaces

    interfaces = regex.findall('Eth\d\/\d+', text)

    # The regular expression to get the numbers of the input

    numbers = regex.findall('(?<!\/|\w)\d+|(?<!\n|-)--', text)

    print(interfaces)

    # Each line of the input has six columns so we group the numbers in groups of six

    numbers_group = [numbers[k:k + 6] for k in range(0, len(numbers), 6)]

    print(numbers_group)

    # Each interface has 3 rows * 6 columns of input --> 18 values of crc counters

    # We separate the values that are associated with each interface and the values missing
    # Are filled with zeros

    k = 0

    numbers_grouped = [numbers_group[k] + numbers_group[k + 4] + numbers_group[k + 6],
                       numbers_group[k + 1] + numbers_group[k + 5] + numbers_group[k + 7],
                       numbers_group[k + 2] + [0] * 12, numbers_group[k + 3] + [0] * 12]

    # Create a dictionary that associates each crc counter with the corresponding value

    eth1_1 = dict(zip(crc_counters, numbers_grouped[k]))
    eth1_2 = dict(zip(crc_counters, numbers_grouped[k + 1]))
    eth1_3 = dict(zip(crc_counters, numbers_grouped[k + 2]))
    eth1_4 = dict(zip(crc_counters, numbers_grouped[k + 3]))

    # Create a list of these dictionaries

    eth_dict = [eth1_1, eth1_2, eth1_3, eth1_4]

    # Create the interfaces dictionary. For each of the input interfaces we associate an empty dictionary.

    interfaces_dict = {intf: {} for intf in interfaces}

    # Put the dictionaries for each interface to the corresponding interface.

    for i, key in enumerate(interfaces_dict.keys()):
        interfaces_dict[key] = eth_dict[i]

    # Print the result

    print(interfaces_dict)




get_error_counters(""""
--------------------------------------------------------------------------------
Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
--------------------------------------------------------------------------------
Eth1/1                1          5          9          13          17           21
Eth1/2                2          6          10          14          18           22
Eth1/3                3          7          11          15          19           23
Eth1/4                4          8          12          16          20           24
<snip>
--------------------------------------------------------------------------------
Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
--------------------------------------------------------------------------------
Eth1/1                25          27          29          31          33           35
Eth1/2                26          28          30          32          34           36
<snip>
--------------------------------------------------------------------------------
Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
--------------------------------------------------------------------------------
Eth1/1                37          --           39           41           43          45
Eth1/2                38          --           40           42           44          46""")
