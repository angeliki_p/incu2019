###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

import re


def is_valid_ip(ip):
    # The regular expression for validating the ip address

    regex = re.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.)"
                       "{3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")

    valid = regex.match(ip)

    # Unless a match is found, valid_flag is false

    valid_flag = False

    if valid is not None:
        valid_flag = True

    return valid_flag


def get_ip_class(ip):
    # Check if input is valid

    valid = is_valid_ip(ip)

    if not valid:

        return_text = "The IP address ' " + ip + " ' is not valid."

    else:

        # Regular expression for class A :

        class_A = re.compile("([1-9]|[1-8][0-9]|9[0-9]|1[01][0-9]|12[0-6])\."
                             "([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\."
                             "([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\."
                             "([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-4])")

        # Regular expression for class B :

        class_B = re.compile("(12[89]|1[3-8][0-9]|19[01])\."
                             "([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\."
                             "([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\."
                             "([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-4])")

        # Regular expression for class C :

        class_C = re.compile('(19[2-9]|2[01][0-9]|22[0-3])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-4])')

        # Regular expression for class D :

        class_D = re.compile('(22[4-9]|23[0-9])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-4])')

        # Regular expression for class E :

        class_E = re.compile('(24[0-9]|25[0-5])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([0-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.'
                             '([1-9]|[1-8][0-9]|9[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-4])')


        if class_A.match(ip) is not None:

            return_text = "The IP address ' " + ip + " ' is class A IP."

        elif class_B.match(ip) is not None:

            return_text = "The IP address ' " + ip + " ' is class B IP."

        elif class_C.match(ip) is not None:

            return_text = "The IP address ' " + ip + " ' is class C IP."

        elif class_D.match(ip) is not None:

            return_text = "The IP address ' " + ip + " ' is class D IP."

        elif class_E.match(ip) is not None:

            return_text = "The IP address ' " + ip + " ' is class E IP."

        else:

            return_text = "The IP address ' " + ip + " ' is classless IP."

    return return_text


is_valid_ip("192.168.10.3")

print(get_ip_class("100.0.2.1"))
