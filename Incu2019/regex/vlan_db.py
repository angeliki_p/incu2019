###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
# 
# How the text input will look like:
# 
# 
# VLAN Name                             Status    Ports
# ---- -------------------------------- --------- -------------------------------
# 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
#                                                 Eth1/4
# 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
#                                                 Eth1/5, Eth1/16, Eth1/17
#                                                 Eth1/18, Eth1/49, Eth1/50
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################

import re


def get_vlan_db(text):

    # Regular expression to find out what are the vlans that exist in our output.
    # IMPORTANT NOTE: For the regular expression to work, we assume a new line
    # after the last line of output.

    regex = re.compile('(?<!\w)\d+(?![\/,\d\n])')

    vlans = regex.findall(text)

    # The output as it seems should contain all the consecutive vlans in the range
    # of the existing vlans. So we make a list that contain vlans 1 - 10 since
    # vlans 1, 10 appear at the output of the command.

    vlan_range = [str(i) for i in range(1, int(vlans[len(vlans) - 1]) + 1)]

    # The regular expression to get Name, Status, Ports

    regex = re.compile('([A-DF-UW-Z][a-z]+(?!\d))')

    keys = regex.findall(text)

    # We also need to take the names of the vlans present at the output.

    regex = re.compile('[^\d\s][A-Za-z]+[\dt]+(?![,\d/\w])')

    names = regex.findall(text)

    # No special regular expression is needed to take the status since all
    # the present vlans on a switch are only the active ones.

    regex = re.compile('active')

    status = regex.findall(text)

    # The regular expression to get the ports associated with the vlans in the output is the following

    regex = re.compile('(?:\s+Eth\d\/\d+[,\s])+')

    ethernet_ports = regex.findall(text)

    # Replace the special characters '\n', commas and multiple spaces that came with the regular expression

    ethernet_ports = [ports.replace("\n", "").replace(" ", "").replace(",", "") for ports in ethernet_ports]

    # After replacing the characters, we need to make the output readable by separating the different
    # occurences.

    ethernet_ports = [re.findall('Eth\d\/\d+', port) for port in ethernet_ports]

    # The regular expression to get the port-channel ports form the output.

    regex = re.compile('(?:Po\d+,\s)+')

    port_channel_ports = regex.findall(text)

    # The output is not as we want it. We remove ',' and spaces and then using regular expressions
    # we separate the occurences.

    port_channel_ports = [ports.replace(",", "").replace(" ", "") for ports in port_channel_ports]

    port_channel_ports = [re.findall('Po\d+', port) for port in port_channel_ports]

    # We need all the ports of each vlan to be merged in one single list rather than in separate ones.

    ports = [x[0] + x[1] for x in zip(ethernet_ports, port_channel_ports)]

    # We create a dictionary with keys in range [1,10] and values an empty dictionary for every key.

    vlan_db = {vlan: {} for vlan in vlan_range}

    # For each vlan present we merge all the information that the output holds for it; names,
    # status and ports.

    vlan_1 = names[:len(names) // 2] + status[:len(status) // 2] \
             + ports[:len(ports) // 2]

    vlan_10 = names[len(names) // 2:] + status[len(status) // 2:] \
              + ports[len(ports) // 2:]

    # We put the data associated with each vlan in one list

    vlan_list = [vlan_1, vlan_10]

    # We use zip() to associate the keys ( Name, Status, Ports)  with the values in the lists
    # we created prior. We create a dictionary for the zip output.

    dicts = [dict(zip(keys, vlan)) for vlan in vlan_list]

    # We use again zip() to associate the vlans (1,10) we have with the dicts we created in the previous step.

    update = dict(zip(vlans, dicts))

    # Finally we update the vlan_db with the respective dictionaries.

    vlan_db.update(update)

    return vlan_db


print(get_vlan_db("""VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
                                                Eth1/4
10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
                                                Eth1/5, Eth1/16, Eth1/17
                                                Eth1/18, Eth1/49, Eth1/50
                                                
"""))
