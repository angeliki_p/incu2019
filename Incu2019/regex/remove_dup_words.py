###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################

import re


def remove_duplicates(text):

    """Removes consecutive duplicate words and replaces them."""

    # We are replacing consecutive duplicate words with "".
    # If no consecutive duplicates are found the string is returned as is.

    regex = re.compile(r'\b(\w+)(?:\s+\1\b)+')

    text_with_no_dup = re.sub(regex, r'', text)

    return text_with_no_dup


print(remove_duplicates("The thing thing is that we are falling apart."))